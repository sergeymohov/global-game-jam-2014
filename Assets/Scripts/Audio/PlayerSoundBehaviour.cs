﻿using UnityEngine;
using System.Collections;

public enum PlayerAnimationState
{
	STEP_LEFT,
	STEP_RIGHT,
	STEP_NONE,
	IDLE
}

public class PlayerSoundBehaviour : MonoBehaviour {

	private PlayerAnimationState curAnimationState = PlayerAnimationState.IDLE;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		AnimatorStateInfo curState = gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);

		if(curState.IsName("Base Layer.Idle") && curAnimationState != PlayerAnimationState.IDLE)
		{
			curAnimationState = PlayerAnimationState.IDLE;
		}

		else if(curState.IsName("Base Layer.Run"))
		{
			if(curState.normalizedTime % 1 > 0.6)
			{
				if(curAnimationState != PlayerAnimationState.STEP_RIGHT)
				{
					//Debug.Log("STEP");
					AkSoundEngine.PostEvent("Play_Step", gameObject);
					curAnimationState = PlayerAnimationState.STEP_RIGHT;
				}
			}

			else if(curState.normalizedTime % 1 > 0.1)
			{
				if(curAnimationState != PlayerAnimationState.STEP_LEFT)
				{
					//Debug.Log("STEP");
					AkSoundEngine.PostEvent("Play_Step", gameObject);
					curAnimationState = PlayerAnimationState.STEP_LEFT;
				}
			}

			else if(curAnimationState != PlayerAnimationState.STEP_NONE)
			{
				curAnimationState = PlayerAnimationState.STEP_NONE;
			}
		}
	}
}
