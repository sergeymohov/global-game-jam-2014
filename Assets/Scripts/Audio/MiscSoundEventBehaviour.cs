﻿using UnityEngine;
using System.Collections;

public class MiscSoundEventBehaviour : MonoBehaviour {

	public string eventName;

	// Use this for initialization
	void Start () {
		AkSoundEngine.PostEvent(eventName, gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
