﻿using UnityEngine;
using System.Collections;

public class AkMultiSourcesPositions : AkGameObject {

	public Transform[] sourcesTransforms;

	// Use this for initialization
	void Start () {

		AkPositionArray positions = new AkPositionArray((uint)sourcesTransforms.Length);
		foreach(Transform sourceTransform in sourcesTransforms)
		{
			positions.Add(sourceTransform.position, sourceTransform.forward);
		}
		AkSoundEngine.SetMultiplePositions(gameObject, positions, (ushort)sourcesTransforms.Length, MultiPositionType.MultiPositionType_MultiSources);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
