﻿using UnityEngine;
using System.Collections;

public enum BirdAnimationState
{
	IDLE,
	FLAP_1,
	FLAP_2,
	FLAP_NONE,
	EAT,
	SHAKE,
	TAKEOFF,
	FLY
}

public class BirdSoundBehaviour : MonoBehaviour {

	private BirdAnimationState curAnimationState = BirdAnimationState.IDLE;

	// Use this for initialization
	void Start () {
		AkSoundEngine.PostEvent("Play_Bird_Tweets", gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		AnimatorStateInfo curState = gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);

		if(curState.IsName("Base Layer.BirdIdle") && curAnimationState != BirdAnimationState.IDLE)
		{
			curAnimationState = BirdAnimationState.IDLE;
		}
		
		else if(curState.IsName("Base Layer.Shake") && curAnimationState != BirdAnimationState.SHAKE)
		{
			curAnimationState = BirdAnimationState.SHAKE;
		}

		else if(curState.IsName("Base Layer.Eat") && curAnimationState != BirdAnimationState.EAT)
		{
			AkSoundEngine.PostEvent("Play_Bird_Eat", gameObject);
			curAnimationState = BirdAnimationState.EAT;
		}

		else if(curState.IsName("Base Layer.TakeOff") && curAnimationState != BirdAnimationState.TAKEOFF)
		{
			AkSoundEngine.PostEvent("Play_Bird_Tweet", gameObject);
			curAnimationState = BirdAnimationState.TAKEOFF;
		}
		
		else if(curState.IsName("Base Layer.Fly"))
		{
			if(curState.normalizedTime % 1 > 0.9)
			{
				if(curAnimationState != BirdAnimationState.FLAP_2)
				{
					Debug.Log("FLAP");
					AkSoundEngine.PostEvent("Play_Bird_Flap", gameObject);
					curAnimationState = BirdAnimationState.FLAP_2;
				}
			}
			
			else if(curState.normalizedTime % 1 > 0.4)
			{
				if(curAnimationState != BirdAnimationState.FLAP_1)
				{
					Debug.Log("FLAP");
					AkSoundEngine.PostEvent("Play_Bird_Flap", gameObject);
					curAnimationState = BirdAnimationState.FLAP_1;
				}
			}
			
			else if(curAnimationState != BirdAnimationState.FLAP_NONE)
			{
				curAnimationState = BirdAnimationState.FLAP_NONE;
			}
		}
	}
}
