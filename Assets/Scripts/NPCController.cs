﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class NPCController : MonoBehaviour 
{
	const string PLAYER_PREFS_TWITTER_USER_ID           = "TwitterUserID";
	const string PLAYER_PREFS_TWITTER_USER_SCREEN_NAME  = "TwitterUserScreenName";
	const string PLAYER_PREFS_TWITTER_USER_TOKEN        = "TwitterUserToken";
	const string PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET = "TwitterUserTokenSecret";
	
	private List<string> BackUpWords = new List<string>();
	
	bool playerInRange = false;

	public float minDistance = 5f;
	
	public bool questIsFollowType = false;
	public bool youTalkingToMe = false; //use this bool if the quest is for the quest-giving npc as target
	
	public string npcName = "Swannounnet";
	public string npcID = "38683264";

	private CController ccontroller;
	private int currentQuestItem = 0;
	
	public string questHandle = "Andipixup";

	[System.NonSerialized]
	public string questWord = "minecraft";

	public List<string> questList = new List<string>();

	private string questCurrent = string.Empty;

	private Transform questPosition;

	public string idlePhrase = "Something about this place is real epic, don't you agree?";

	public GameObject npcPrefab;
	
	private bool firstUpdate = true;

	[System.NonSerialized]
	public bool questComplete = false;
	[System.NonSerialized]
	public bool questGiven = false;
	
	[System.NonSerialized]
	public Twitter.User locUser = new Twitter.User();
	
	private ExclamationMark eMark;
	
	private GameObject NPCPopup;
	private PopUpButton questButton;

	private PopUp popUp;

	public bool friendQuest;

	void Start()
	{
		BackUpWords.Add("government");
		BackUpWords.Add("economy");
		BackUpWords.Add("recession");
		BackUpWords.Add("rebel");
		BackUpWords.Add("potato");
		BackUpWords.Add("alpaca");
		BackUpWords.Add("banana");
		BackUpWords.Add("bluebird");
		BackUpWords.Add("bottle");
		BackUpWords.Add("meltdown");
		BackUpWords.Add("market");
		BackUpWords.Add("sand");
		BackUpWords.Add("mushroom");
		BackUpWords.Add("disaster");
		BackUpWords.Add("rock");
		BackUpWords.Add("debt");
		BackUpWords.Add("candies");
		BackUpWords.Add("saga");
		BackUpWords.Add("crush");
		BackUpWords.Add("crusher");
		BackUpWords.Add("bridge");
		
		if (Application.isPlaying)
		{
			if (transform.FindChild("Exclamation_mark"))
			eMark = transform.FindChild("Exclamation_mark").GetComponent<ExclamationMark>();

			if (transform.FindChild("NPCPopUp"))
			popUp = transform.FindChild("NPCPopUp").GetComponent<PopUp>();
		}

		ccontroller = CController.instance;
		
		//spawn point is where the target NPC for the quest is
		questPosition = transform.FindChild("SpawnPoint");
		questPosition.renderer.enabled = false;

		
		NPCPopup = transform.FindChild("NPCPopUp").gameObject;
		for(int i = 0; i < NPCPopup.transform.GetComponentsInChildren<PopUpButton>().Length; i++)
		{
			if (NPCPopup.transform.GetComponentsInChildren<PopUpButton>()[i].name == "Quest")
			{
				questButton = NPCPopup.transform.GetComponentsInChildren<PopUpButton>()[i];
				break;
			}
		}
		
		//Demo.instance.getFriend(1
		
		//gameObject.transform.FindChild("NPCPopUp").renderer.enabled = false;
		StartCoroutine("BoredCycle");

	}

	void Update () 
	{
		if(Application.isPlaying && firstUpdate && Demo.instance.friendLoaded && !InstantiateSinceBegining){
			Debug.Log("I CHOSE YOU PIKACHU - seriously, friend / "+InstantiateSinceBegining);

			Debug.Log(Demo.instance.friends.Count);
			
			for(int i=0; i<Demo.instance.friends.Count; i++)
			{
				if(Demo.instance.friends[""+i].Id == npcID)
				{
					locUser = Demo.instance.friends[""+i];
					break;
				}
			}
			

			npcID = locUser.Id;
			npcName = locUser.Name;
			
			Debug.Log(npcID+"/"+npcName);

			StartCoroutine(Twitter.API.SearchUserId(npcID, 
				Demo.instance.CONSUMER_KEY, 
				Demo.instance.CONSUMER_SECRET, 
				Demo.instance.m_AccessTokenResponse, 
				new Twitter.SearchUserCallback(this.OnSearchUsers)));

			//UpdateQuestText();

			firstUpdate = false;
		}else if(Application.isPlaying && firstUpdate && Demo.instance.TVeopleLoaded && InstantiateSinceBegining) {
			
			Debug.Log("I CHOSE YOU PIKACHU - seriously, Tveople / "+InstantiateSinceBegining);
			locUser = Demo.instance.tveople[Random.Range(0, Demo.instance.tveople.Count)];
			
			npcID = locUser.Id;
			npcName = locUser.Name;
			

			StartCoroutine(Twitter.API.SearchUserId(npcID, 
				Demo.instance.CONSUMER_KEY, 
				Demo.instance.CONSUMER_SECRET, 
				Demo.instance.m_AccessTokenResponse, 
				new Twitter.SearchUserCallback(this.OnSearchUsers)));
			
			firstUpdate = false;
		}
		
		if (!Application.isPlaying && !CController.instance)
		{
			ccontroller = GameObject.Find("Player").GetComponent<CController>();
		}

		gameObject.name = "NPC_" + npcName;

		if (Application.isPlaying)
		{
			playerInRange = (Vector3.Distance(CController.instance.transform.position, transform.position) <= minDistance) ? true : false;

			if (!playerInRange)
			{
				if (popUp)
				popUp.enable = false;

				if (questList.Count > 0)
				{
					if(eMark)
					eMark.ExclamationShow();
				}
				else
				{
					if(eMark)
					eMark.ExclamationHide();
				}
			}
			else
			{
				if (popUp)
				popUp.enable = true;
				if(eMark)
				eMark.ExclamationHide();
			}

			//TODO: show interaction interface here
			/*if (questCurrent != "" && !questGiven && Input.GetKeyUp(KeyCode.P))
			{
				//TODO: replace by popup

				//increments the index of the quest thingy
				UpdateQuestIndex();

				if (questGiven)
				{
					GameObject spawnNPC = (GameObject)Instantiate(npcPrefab);
					spawnNPC.transform.position = questPosition.position;
				}
			}*/

			if (idlePhrase != "" && Input.GetKeyUp(KeyCode.O))
			{
				//TODO: replace by popup
				Debug.Log(idlePhrase);
			}
		}
	}

	private bool firstClickButton = true;

	void QuestButtonPressed()
	{
		if(firstClickButton)
		{
			firstClickButton = false;

			string myKey = "";
			
			//Debug.Log("step2");
			if(!youTalkingToMe)
			{
				GameObject spawnNPC = (GameObject)Instantiate(npcPrefab);
				spawnNPC.transform.position = questPosition.position;
				myKey = ""+Random.Range(0, Demo.instance.friends.Count);
				spawnNPC.SendMessage("WasSpawned", Demo.instance.friends[myKey].Id);
			}
			//Debug.Log(myKey);
			
			
			
			if(!questIsFollowType)
			{		
				string keyword = "";
				
				string[] stringSeparators = new string[] {" "};
				

				Debug.Log("debug + "+locUser.LastTweet.text+"/"+locUser.Name);
				string[] words = locUser.LastTweet.text.Split(stringSeparators, System.StringSplitOptions.None);
				
				for(int i=0; i<words.Length; i++)
				{
					if (words[i].Length > 4) 
					{
						if(!words[i].Contains("@")
							&& !words[i].Contains("#")
							&& !words[i].Contains(",")
							&& !words[i].Contains(".")
							&& !words[i].Contains("!")
							&& !words[i].Contains(":")
							&& !words[i].Contains(";")
							&& !words[i].Contains("'")
							&& !words[i].Contains("?")
							&& !words[i].Contains("(")
								&& !words[i].Contains("-")
								&& !words[i].Contains(")")
								&& !words[i].Contains("+")
								&& !words[i].Contains("=")){
							keyword = words[i];
							break;
						}
					}
				}
				
				if(keyword == ""){
					keyword = BackUpWords[Random.Range(0, BackUpWords.Count)];
				}
				
				questWord = keyword.ToLower();
				
				eMark.ExclamationKill();
				questButton.enable = false;
				
				
				
				//Debug.Log("gave a quest "+questWord);
				if(!youTalkingToMe)
				{
					Demo.instance.AddPlayerQuest(Demo.instance.friends[myKey].Name, questWord);
					questHandle = Demo.instance.friends[myKey].Name;
				} else {
					Demo.instance.AddPlayerQuest(locUser.Name, questWord);
					questHandle = locUser.Name;
				}
				
			} else if (!friendQuest) {
				//Debug.Log("gave a follow quest ");
				if(!youTalkingToMe)
				{
					Demo.instance.AddFollowQuest(Demo.instance.friends[myKey].Id);
					questHandle = Demo.instance.friends[myKey].Name;
				} else {
					Demo.instance.AddFollowQuest(locUser.Id);
					questHandle = locUser.Name;
				}
				
			}
			else
			{
				if(!youTalkingToMe)
				{
					Demo.instance.AddFriendQuest(Demo.instance.friends[myKey].Name, questWord);
					questHandle = Demo.instance.friends[myKey].Name;
				} else {
					Demo.instance.AddFriendQuest(locUser.Name, questWord);
					questHandle = locUser.Name;
				}
			}

		}
		
		
		AkSoundEngine.PostEvent("Play_NPC_Speech", gameObject);
		//Debug.Log("I want to give a quest");
		if (questCurrent != "" && !questGiven)
		{
			BroadcastMessage("UpdateOutput", UpdateQuestText(questCurrent), SendMessageOptions.DontRequireReceiver);
			UpdateQuestIndex();
			//Debug.Log("step1");

			if (questGiven)
			{
				


					//Debug.Log("NPC SPawned");
				
			} else {

			}
			
			
		}

		currentQuestItem++;
		
		
		if(currentQuestItem >= questList.Count)
		{
			BroadcastMessage("Last");
		}
	}

	// public void ResetQuestPages() {
	// 	currentQuestItem = 0;
	// }
	
//	  void QuestButtonPressed()
//        {
//                Debug.Log("I want to give a quest");
//                if (questCurrent != "" && !questGiven)
//                {
//                        BroadcastMessage("UpdateOutput", UpdateQuestText(questCurrent), SendMessageOptions.DontRequireReceiver);
//                        UpdateQuestIndex();
// 
//                        if (questGiven)
//                        {
//                                BroadcastMessage("Last");
//                                GameObject spawnNPC = (GameObject)Instantiate(npcPrefab);
//                                spawnNPC.transform.position = questPosition.position;
//                                string myKey = ""+Random.Range(0, Demo.instance.friends.Count);
//                                        Debug.Log(myKey);
//                                        spawnNPC.SendMessage("WasSpawned", Demo.instance.friends[myKey].Id);
//                               
//                                        Debug.Log("NPC SPawned");
//                               
//                        }
//                }
//        }
	
	public bool InstantiateSinceBegining = false;
	
	void OnLevelWasLoaded()
	{
		InstantiateSinceBegining = true;
	}
	
	public void WasSpawned(string user_id)
	{
		//Debug.Log("I was spawned and I wouldn't have it any other way. "+user_id);
		npcID = user_id;
	}

	void UpdateQuestIndex()
	{
		foreach (string str in questList)
		{
			if (questCurrent == str)
			{
				if (questList.Count-1 > questList.IndexOf(str)){
					questCurrent = questList[questList.IndexOf(str)+1];
					UpdateQuestText(questCurrent);
					//Debug.Log("MEGA IMPORTANT DE LA MORT  "+questList.IndexOf(str)+"/"+questList.Count);
				}
				else
				{questGiven = true;}


				break;
			}
		}
	}

	string UpdateQuestText(string _strVariable)
	{
		if (_strVariable == "")
		_strVariable = string.Format("Make {0} say {1}.", questHandle, questWord);
		else 
		{
				//1 = word
				//0 = handle

			Debug.Log("DEBUG ******* +" + questWord);
			// if (_strVariable.Contains("{0}") && !_strVariable.Contains("{1}"))
			// _strVariable = string.Format(_strVariable, questHandle);
			// else if (_strVariable.Contains("{1}") && !_strVariable.Contains("{0}"))
			// _strVariable = string.Format(_strVariable, questWord);
			// else if (_strVariable.Contains("{1}") && _strVariable.Contains("{0}"))
			// _strVariable = string.Format(_strVariable, questHandle, questWord);
			
			if (_strVariable.Contains("%username%"))
			{ 
				_strVariable=_strVariable.Replace("%username%", "@"+PlayerPrefs.GetString("TwitterUserScreenName"));
			}

			if (_strVariable.Contains("{0}"))
			{ 
				_strVariable=_strVariable.Replace("{0}", questHandle);
			}

			if (_strVariable.Contains("{1}"))
			{ 
				_strVariable=_strVariable.Replace("{1}", questWord);
			}

		}
		return _strVariable;
	}


	IEnumerator BoredCycle()
	{
		yield return new WaitForSeconds(Random.Range(3f, 17f));

		gameObject.GetComponent<Animator>().SetBool("DoBored", true);
	}

	public void OnEndBored()
	{
		gameObject.GetComponent<Animator>().SetBool("DoBored", false);

		StartCoroutine("BoredCycle");
	}
	
	void OnSearchUsers(bool success, JSONNode rootNode)
	{
		
		if(!success) return;

		string url = rootNode[0]["profile_image_url"];
		
		print("Loading my own avatar OnSearchUsers in NPC CONTROLLER - " + (success ? "succeeded." : "failed.") + " /// "+url);
		
		if (questList.Count > 0)
		{
			questCurrent = questList[0];
			questCurrent = UpdateQuestText(questCurrent);
		}	

		StartCoroutine(LoadAvatar(url, transform.FindChild("MOD_ALL").FindChild("TV_head").renderer.materials[1]));		
		
		
	}
	
	public IEnumerator LoadAvatar(string localURL, Material mat) {
		Debug.Log("loading avatar from "+localURL);
		
		WWW www = new WWW(localURL);
		yield return www;
		
		mat.mainTexture = www.texture;
	}
	

}
