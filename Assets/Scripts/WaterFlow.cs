﻿using UnityEngine;
using System.Collections;

public class WaterFlow : MonoBehaviour {

	public Vector2 flowSpeed = new Vector2(-0.005f, -0.005f);
	private Vector2 offset = new Vector2(0f, 0f);

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		offset += flowSpeed;
		renderer.material.SetTextureOffset("_MainTex", offset);
	}
}
