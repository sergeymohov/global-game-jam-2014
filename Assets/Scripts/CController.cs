﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class CController : Singleton<CController> 
{
	[System.NonSerialized]
	public int candiesCollected = 0;

	float speed = 10f;
	Vector3 direction;
	Vector3 resultingVelocity = Vector3.zero;
	Vector3 additionalVelocity = Vector3.zero;
	bool firstUpdate = true;
	
	bool allowMovement = true;

	public Material tempMat;
	
	const string PLAYER_PREFS_TWITTER_USER_ID           = "TwitterUserID";
    const string PLAYER_PREFS_TWITTER_USER_SCREEN_NAME  = "TwitterUserScreenName";
    const string PLAYER_PREFS_TWITTER_USER_TOKEN        = "TwitterUserToken";
    const string PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET = "TwitterUserTokenSecret";

	void Start()
	{
		gameObject.GetComponent<Animator>().SetBool("DoRun", false);
		
		StartCoroutine("BoredCycle");
	}

	IEnumerator BoredCycle()
	{
		yield return new WaitForSeconds(Random.Range(3f, 17f));

		gameObject.GetComponent<Animator>().SetBool("DoBored", true);
	}

	public void OnEndBored()
	{
		gameObject.GetComponent<Animator>().SetBool("DoBored", false);

		StartCoroutine("BoredCycle");
	}

	public void OnEndRun()
	{
		gameObject.GetComponent<Animator>().SetBool("DoRun", false);
		OnEndBored();
	}

	void FixedUpdate () 
	{	
		
		
		if(firstUpdate){
			Debug.Log(Demo.instance.m_AccessTokenResponse);
			Debug.Log(Demo.instance.m_AccessTokenResponse.UserId);
			
			Debug.Log("mtokenresponse: --- "+Demo.instance.m_AccessTokenResponse);
			StartCoroutine(Twitter.API.SearchUserId(PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_ID ), 
					Demo.instance.CONSUMER_KEY, 
					Demo.instance.CONSUMER_SECRET, 
					Demo.instance.m_AccessTokenResponse, 
					new Twitter.SearchUserCallback(this.OnSearchUsers)));
			
			firstUpdate = false;
		}
		
		direction = Vector3.zero;

		if (allowMovement)
		{
			if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow))
			{			
				direction += Vector3.right;
				gameObject.GetComponent<Animator>().SetBool("DoRun", true);
			}
			
			if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
			{			
				direction += Vector3.left;
				gameObject.GetComponent<Animator>().SetBool("DoRun", true);
			}
			
			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
			{
				direction += Vector3.forward;
				gameObject.GetComponent<Animator>().SetBool("DoRun", true);
			}
			
			if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
			{
				direction += Vector3.back;
				gameObject.GetComponent<Animator>().SetBool("DoRun", true);
			}

			gameObject.transform.forward = Vector3.Lerp(gameObject.transform.forward, direction, 4 * Time.fixedDeltaTime);
			
			Vector3 playerVelocity = (speed + additionalVelocity.x) * direction;

			rigidbody.MovePosition(rigidbody.position + playerVelocity * Time.deltaTime);

			resultingVelocity = playerVelocity;
		}
	}

	public void StopController()
	{
		if (allowMovement) allowMovement = false;
		AkSoundEngine.PostEvent("Dim_Music", Camera.main.gameObject);
	}

	public void RestartController()
	{
		if (!allowMovement) allowMovement = true;
		AkSoundEngine.PostEvent("Restore_Music", Camera.main.gameObject);
	}

	#region handling twitter link
	
	void OnSearchUsers(bool success, JSONNode rootNode)
	{
		
		
		print("Loading my own avatar OnSearchUsers - " + (success ? "succeeded." : "failed."));
		
		if(!success) return;

		string url = rootNode[0]["profile_image_url"];
       
		StartCoroutine(LoadAvatar(url, tempMat));				
	}
	
	public IEnumerator LoadAvatar(string localURL, Material mat) {
		Debug.Log("loading avatar from "+localURL);
		
        WWW www = new WWW(localURL);
        yield return www;
		
		mat.mainTexture = www.texture;
      }
	
	#endregion
}