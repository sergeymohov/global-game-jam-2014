﻿using UnityEngine;
using System.Collections;

public class ListenButton : AbstractButton {

	public void Start() {
		base.Start();
		param = new GUISizer.GUIParams(pos, "Listen");
	}

	public override void Begin() {
		
	}

	public override void ButtonEffects() {
		Debug.Log(Demo.instance.friends["38683264"].LastTweet.text);
		//SendMessageUpwards("TalkButtonPressed");
	}
}
