﻿using UnityEngine;
using System.Collections;

public class ExitButton : AbstractButton {

	public void Start() {
		base.Start();
		if(name == "")
			name = "Close";
		param = new GUISizer.GUIParams(pos, name);
	}

	public override void Begin() {
		
	}

	public override void ButtonEffects() {
		targetPopUp.Exit();
		CController.instance.RestartController();
	}
}
