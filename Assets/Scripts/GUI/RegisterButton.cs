﻿using UnityEngine;
using System.Collections;

public class RegisterButton : AbstractButton {

	private GUISizer.GUIParams registerButton = new GUISizer.GUIParams(GUISizer.PositionDef.topMiddle, "Register");
	private GUISizer.GUIParams pinButton = new GUISizer.GUIParams(GUISizer.PositionDef.bottomRight, "PIN");
	private GUISizer.GUIParams keyField = new GUISizer.GUIParams(GUISizer.PositionDef.bottomMiddle, "");
	public string CONSUMER_KEY = "2ajUtA29vj5iY5khWeXG9A";
	public string CONSUMER_SECRET = "mhssmGApyVGTogwjbGUj9XqTUlu4VLtZgFgXKH3LQ";
	private string pin = "";
	private Twitter.RequestTokenResponse m_RequestTokenResponse;
    private Twitter.AccessTokenResponse m_AccessTokenResponse;
    const string PLAYER_PREFS_TWITTER_USER_ID           = "TwitterUserID";
    const string PLAYER_PREFS_TWITTER_USER_SCREEN_NAME  = "TwitterUserScreenName";
    const string PLAYER_PREFS_TWITTER_USER_TOKEN        = "TwitterUserToken";
    const string PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET = "TwitterUserTokenSecret";
	
    public override void Begin() {
        
    }


	public void Start(){
        base.Start();
		Twitter.AccessTokenResponse m_AccessTokenResponse = new Twitter.AccessTokenResponse();

        m_AccessTokenResponse.UserId        = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_ID);
        m_AccessTokenResponse.ScreenName    = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME);
        m_AccessTokenResponse.Token         = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN);
        m_AccessTokenResponse.TokenSecret   = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET);

        if (!string.IsNullOrEmpty(m_AccessTokenResponse.Token) &&
            !string.IsNullOrEmpty(m_AccessTokenResponse.ScreenName) &&
            !string.IsNullOrEmpty(m_AccessTokenResponse.Token) &&
            !string.IsNullOrEmpty(m_AccessTokenResponse.TokenSecret))
        {
            string log = "LoadTwitterUserInfo - succeeded";
            log += "\n    UserId : " + m_AccessTokenResponse.UserId;
            log += "\n    ScreenName : " + m_AccessTokenResponse.ScreenName;
            log += "\n    Token : " + m_AccessTokenResponse.Token;
            log += "\n    TokenSecret : " + m_AccessTokenResponse.TokenSecret;
            print(log);
			
			Application.LoadLevel("Scene0");
        }
		
	}
	
    public override void ButtonEffects() {}

	public override void OnGUI() {
		if(targetPopUp.enable) {
			GUISizer.BeginGUI();
			if (GUISizer.ButtonPressed(registerButton))
				StartCoroutine(Twitter.API.GetRequestToken(CONSUMER_KEY, CONSUMER_SECRET,
                                                           new Twitter.RequestTokenCallback(OnRequestTokenCallback)));

	        pin = GUISizer.TextField(keyField, pin);

	        if (GUISizer.ButtonPressed(pinButton))
	        {
	            StartCoroutine(Twitter.API.GetAccessToken(CONSUMER_KEY, CONSUMER_SECRET, m_RequestTokenResponse.Token, pin,
	                           new Twitter.AccessTokenCallback(OnAccessTokenCallback)));
	        }
			GUISizer.EndGUI();
		}
	}

	void OnRequestTokenCallback(bool success, Twitter.RequestTokenResponse response)
    {
		pin = "Callback";
        if (success)
        {
			pin = "Success Callback";
            string log = "OnRequestTokenCallback - succeeded";
            log += "\n    Token : " + response.Token;
            log += "\n    TokenSecret : " + response.TokenSecret;
            print(log);

            m_RequestTokenResponse = response;

            Twitter.API.OpenAuthorizationPage(response.Token);
        }
        else
        {
			pin = "Failed callback";
            print("OnRequestTokenCallback - failed.");
        }
    }

    void OnAccessTokenCallback(bool success, Twitter.AccessTokenResponse response)
    {
        if (success)
        {
            string log = "OnAccessTokenCallback - succeeded";
            log += "\n    UserId : " + response.UserId;
            log += "\n    ScreenName : " + response.ScreenName;
            log += "\n    Token : " + response.Token;
            log += "\n    TokenSecret : " + response.TokenSecret;
            print(log);
		
            m_AccessTokenResponse = response;

            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_ID, response.UserId);
            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME, response.ScreenName);
            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_TOKEN, response.Token);
            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET, response.TokenSecret);
			
			//Player authenticated
			//at that point we can go to the main scene
			Application.LoadLevel("Scene0");
        }
        else
        {
            print("OnAccessTokenCallback - failed.");
        }
    }

}
