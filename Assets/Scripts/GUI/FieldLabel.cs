﻿using UnityEngine;
using System.Collections;

public class FieldLabel : AbstractInOut {

	protected GUISizer.GUIParams txtLabel;
	protected PopUp targetPopUp;

	public virtual void Start(){
		base.Start();
		txtLabel = new GUISizer.GUIParams(pos, "");
		txtLabel.x -= 150;
		txtLabel.width += 150;
		txtLabel.y -= 150;
		txtLabel.height += 150;
		targetPopUp = this.gameObject.GetComponent<PopUp>();
		if(targetPopUp == null) {
			Debug.Log("Error: you must attach a PopUp component to your game object.");
		}
	}

	public void OnGUI() {
		if(targetPopUp.IsSleeping()) {
			GUI.depth = 2;
		}
		else {
			GUI.depth = 0;
		}
		if(targetPopUp.enable && targetPopUp != null) {
			if(targetToFollow == null) {
				GUISizer.BeginGUI();
				targetPopUp.output = GUISizer.TextField(txtLabel, targetPopUp.output);
				GUISizer.EndGUI();
			} 
			else {
				if(bgImg == null)
					GUI.Label(GetRectLabel(), targetPopUp.output);
				else {
					targetPopUp.output = GUISizer.TextField(txtLabel, targetPopUp.output);
					GUI.Label(GetRectLabel(), targetPopUp.output, style);
				}
			}
		}
	}

	public override void Begin() 
	{
		if (!transform.parent.GetComponent<NPCController>()) return;

		targetPopUp.output = "@"+transform.parent.GetComponent<NPCController>().npcName;
	}
}
