﻿using UnityEngine;
using System.Collections;

public class AcceptButton : AbstractButton {

	public bool accepted = false;

	public void Start() {
		base.Start();
		param = new GUISizer.GUIParams(pos, "Accept");
	}
	
	public override void Begin() {
		
	}

	public override void ButtonEffects() {
		accepted = true;
		targetPopUp.Exit();
	}	

}
