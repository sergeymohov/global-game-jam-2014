﻿using UnityEngine;
using System.Collections;

public class NextButton : AbstractButton {

	private bool last = false;
	public PopUp popUp;
	
	private int testInt = 0;

	public void Start() {
		base.Start();
		if(name == "")
			name = "Next";
		param = new GUISizer.GUIParams(pos, name);
	}

	public override void ButtonEffects() {
		if(!last){
			SendMessageUpwards("QuestButtonPressed");
			if(last){
				enable = false;
				CController.instance.RestartController();}
		}
		else {
			enable = false;
			CController.instance.RestartController();
		}
	}

	public override void Begin() {
		
	}

	public void Last() {
		last = true;
	}


}
