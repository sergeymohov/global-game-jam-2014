﻿using UnityEngine;
using System.Collections;

public class TextLabel : AbstractInOut {

	protected GUISizer.GUIParams txtLabel;
	protected PopUp targetPopUp;

	public virtual void Start(){
		base.Start();
		txtLabel = new GUISizer.GUIParams(pos, "");
		txtLabel.x -= 150;
		txtLabel.width += 150;
		txtLabel.y -= 150;
		txtLabel.height += 150;

		style.wordWrap = true;
		
		targetPopUp = this.gameObject.GetComponent<PopUp>();
		if(targetPopUp == null) {
			Debug.Log("Error: you must attach a PopUp component to your game object.");
		}

	}

	public void OnGUI() {
		if(targetPopUp.IsSleeping()) {
			GUI.depth = 2;
		}
		else {
			GUI.depth = 0;
		}
		if(targetPopUp.enable && targetPopUp != null) {
			if(targetToFollow == null) {
				GUISizer.BeginGUI();
				style.normal.textColor = Color.black;
				style.fontSize = 40;
				GUISizer.MakeLabel(txtLabel, style.fontSize, style, targetPopUp.output);
				GUISizer.EndGUI();
			} 
			else {
				if(bgImg == null)
					GUI.Label(GetRectLabel(), targetPopUp.output);
				else {
					GUI.Label(GetRectLabel(), bgImg, style);
					GUI.Label(GetRectLabel(), targetPopUp.output, style);
				}
			}
		}
	}

	public override void Begin() {
		
	}

}
