﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SendTweetButton : AbstractButton {

	private PopUp popup;
	public PopUp nextPopupLol;
	
	private GameObject myPlayer;
	private PopUp scorePopup;

	public void Start() {
		base.Start();
		if(name == "")
			name = "Tweet";
		param = new GUISizer.GUIParams(pos, name);
		popup = this.gameObject.GetComponent<PopUp>();

		Debug.Log("????????????????????????????????????????????");
		myPlayer = GameObject.Find("Player").transform.FindChild("VictoryQuestPopUp").gameObject;
		//Debug.Log(myPlayer);
		scorePopup = myPlayer.GetComponent<PopUp>();

		Demo.instance.scoreScreen = scorePopup;
		//Debug.Log(scorePopup);
	}

	public override void Begin() {

	}

	public override void ButtonEffects() 
	{
		if (popup.output == "") return;

		SendMessageUpwards("TweetButtonPressed", SendMessageOptions.DontRequireReceiver);

		Demo.instance.Tweet(popup.output);

		Demo.instance.CheckFriendQuests();
		
		//Check Quests
		Demo.instance.CheckPlayerQuestStatus(callbackCheck);
		
		CController.instance.RestartController();
		targetPopUp.Exit();

		if (nextPopupLol)
			nextPopupLol.WakeUp();
	}
	
	public void callbackCheck(List<string> questTargets, List<string> questKeywords)
	{
		if(questTargets.Count > 0) {
			
			scorePopup.Begin();
			scorePopup.WakeUp();

			int locScore= 0 ;

			for(int i = 0; i<questTargets.Count; i++)
			{
				for(int u = 0; u < Demo.instance.friends.Count; u++)
				{
					if(Demo.instance.friends[u+""].Name.ToLower() == questTargets[i])
					{
						locScore += int.Parse(Demo.instance.friends[u+""].Followers);
						break;
					}
				}
			}
		
			scorePopup.enable = true;
			scorePopup.output = "You completed "+questTargets.Count+" TVittQuest and won "+locScore+" candies";

			CController.instance.candiesCollected += locScore;


			Debug.Log("SendTweetButton.Quest Completed");
			
			
		
		}
		else {Debug.Log("No Quest Completed");}
	}
}
