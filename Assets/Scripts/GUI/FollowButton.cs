﻿using UnityEngine;
using System.Collections;

public class FollowButton : AbstractButton {

	private GameObject myPlayer;
	private PopUp scorePopup;


	public void Start() {
		base.Start();
		if(name == "")
			name = "Follow";
		param = new GUISizer.GUIParams(pos, name);

		myPlayer = GameObject.Find("Player").transform.FindChild("VictoryQuestPopUp").gameObject;
		//Debug.Log(myPlayer);
		scorePopup = myPlayer.GetComponent<PopUp>();

	}

	public override void ButtonEffects() {
		enable = false;

		Demo.instance.Follow(transform.parent.GetComponent<NPCController>().npcName);

		if(Demo.instance.followQuestTargets.IndexOf(transform.parent.GetComponent<NPCController>().npcID) >= 0)
		{
			scorePopup.enable = true;
			scorePopup.output = "You completed a TVollow TVittQuest and won "+(35+Random.Range(0,15))+" candies";
			CController.instance.candiesCollected += 35;
		}
	}

	public override void Begin() {
		
	}

}
