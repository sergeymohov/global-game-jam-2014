﻿using UnityEngine;
using System.Collections;

public class PopUp : MonoBehaviour {
	public bool enable = true; //Rendering
	protected GUISizer.GUIParams window = new GUISizer.GUIParams(GUISizer.PositionDef.middle, "");
	public PopUp nextPopUp; // When clicking on next
	protected bool exitable = true;
	public string output; //Output
	public GameObject targetToFollow;
	public bool fullScreen = true;
	public float width;
	public float height;
	private Camera mainCamera;
	private bool sleeping = false;
	public Texture bg;

	public void Start() 
	{
		mainCamera = Camera.main;

		if(fullScreen) 
		{
			width = Screen.width;
			height = Screen.height;
		}

		if (!bg && gameObject.name == "TalkPopUp")
		{
			bg = (Texture)Resources.Load("UI_Panel");
		}
	}

	public void Sleep() {
		sleeping = true;
	}

	public void WakeUp() {
		sleeping = false;
	}

	public bool IsSleeping() {
		return sleeping;
	}

	public void Update() {
		if(targetToFollow != null) {
			window.x = mainCamera.WorldToScreenPoint(targetToFollow.transform.position).x-width;
			window.y = mainCamera.pixelHeight-mainCamera.WorldToScreenPoint(targetToFollow.transform.position).y-height;
			AbstractInOut[] arr = GetComponents<AbstractInOut>();
			Rect r = new Rect(window.x, window.y, width, height);
			foreach(AbstractInOut compo in arr) {
				compo.SetTargetToFollow(targetToFollow);
				compo.SetBorders(r);
			}
		}

		// 140 caracteres
		if(output.Length > 140) {
			string tmp = "";
			for(int i = 0; i < 140; i++) {
				tmp += output[i];
			}
			output = tmp;
		}
	}

	public void OnGUI() {
		if(!enable)
			return;
		if(fullScreen) {
			GUI.Label(new Rect(0, 0, GUISizer.WIDTH, GUISizer.HEIGHT), bg);
		}
	}

	public void Exit() {
		enable = false;
		CController.instance.RestartController();
		gameObject.SendMessageUpwards("ResetQuestCounter", SendMessageOptions.DontRequireReceiver);
		// SendMessageUpwards("ResetQuestPages");
	}

	public void Begin() {
		enable = true;
		WakeUp();
		AbstractInOut[] arr = GetComponents<AbstractInOut>();
		foreach(AbstractInOut compo in arr) {
			compo.Begin();
		}
	}

	public void Next() {
		Exit();
		if(nextPopUp != null)
			nextPopUp.Begin();
		else
			Debug.Log("Error: no next pop up assigned.");
	}

	public void UpdateOutput(string msg) {
		output = msg;
	}

}
