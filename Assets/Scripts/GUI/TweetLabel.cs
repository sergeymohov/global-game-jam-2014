﻿using UnityEngine;
using System.Collections;

public class TweetLabel : TextLabel {

	public override void Begin() {
		if (!transform.parent.GetComponent<NPCController>()) return;
		AkSoundEngine.PostEvent("Play_NPC_Speech", transform.gameObject);
		if(transform.parent.GetComponent<NPCController>().idlePhrase != "")
		{
			targetPopUp.UpdateOutput(transform.parent.GetComponent<NPCController>().idlePhrase);
		}else{
			targetPopUp.UpdateOutput(transform.parent.GetComponent<NPCController>().locUser.LastTweet.text); // <-- mets ton tweet dans cette string
		}	

	}
}
