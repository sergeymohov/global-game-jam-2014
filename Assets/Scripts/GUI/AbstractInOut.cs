﻿using UnityEngine;
using System.Collections;

public abstract class AbstractInOut : MonoBehaviour {
	public GUISizer.PositionDef pos = GUISizer.PositionDef.topLeft;
	protected GUISizer.GUIParams param;
    public Texture bgImg;
    public GUIStyle style;

	// Absolute datas
	protected GameObject targetToFollow;
	protected Rect borders;
	public Vector2 altSize = new Vector2(100, 25);

    public void Start() {
        style.font = Resources.Load("Fonts/HelveticaNeueLTStd-Th") as Font;
        Debug.Log(style.font);
        style.alignment = TextAnchor.LowerCenter;
    }

	public void SetTargetToFollow(GameObject target) {
		targetToFollow = target;
	}

	public void SetBorders(Rect r) {
		borders = r;
	}

    public abstract void Begin();

	public Rect GetRectLabel() {
		Rect tmpR = borders;
		tmpR.width = altSize.x;
		tmpR.height = altSize.y;
		switch(pos)
        {
            case GUISizer.PositionDef.topLeft:
    			tmpR.y -= tmpR.height*2;
            	break;
            case GUISizer.PositionDef.topMiddle:
            	tmpR.y -= tmpR.height*2;
        		tmpR.x += tmpR.width/2;
                break;
            case GUISizer.PositionDef.topRight:
        		tmpR.y -= tmpR.height*2;
        		tmpR.x += tmpR.width;
                break;
            case GUISizer.PositionDef.bottomLeft:
                tmpR.x -= tmpR.width/2;
                break;
            case GUISizer.PositionDef.bottomMiddle:
	            tmpR.x += tmpR.width/2;
    			break;
            case GUISizer.PositionDef.bottomRight:
        		tmpR.x += tmpR.width;
                break;
            case GUISizer.PositionDef.left:
            	tmpR.y -= tmpR.height;
                break;
            case GUISizer.PositionDef.right:
            	tmpR.y -= tmpR.height;
            	tmpR.x += tmpR.width;
                break;
            case GUISizer.PositionDef.middle:
            	tmpR.y -= tmpR.height;
        		tmpR.x += tmpR.width/2;
                break;
    	}
		
		tmpR.y -= 50;
    	return tmpR;
	}
}
