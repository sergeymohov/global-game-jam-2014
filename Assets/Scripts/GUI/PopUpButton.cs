﻿using UnityEngine;
using System.Collections;

public class PopUpButton : AbstractButton {

	public PopUp popUp;

	public void Start() {
		base.Start();
		if(name == "")
			name = targetPopUp.output;
		else if (name == "Close")
		{
			if (transform.parent.name == "TweetBird")
			{
				popUp = transform.parent.FindChild("NPCPopUp").GetComponent<PopUp>();
			}
		}

		
		param = new GUISizer.GUIParams(pos, name);
	}

	public override void ButtonEffects() {
		targetPopUp.nextPopUp = popUp;
		if(!targetPopUp.fullScreen)
			targetPopUp.Sleep();
		else
			targetPopUp.enable = false;
		targetPopUp.nextPopUp.Begin();
		if(name == "Close") {
			CController.instance.RestartController();
		}
		else if(name == "Quest") {
			SendMessageUpwards("QuestButtonPressed");
			CController.instance.StopController();
		}
		else
			CController.instance.StopController();
		//SendMessageUpwards("NextButtonPressed");
	}

	public override void Begin() {
		
	}

}
