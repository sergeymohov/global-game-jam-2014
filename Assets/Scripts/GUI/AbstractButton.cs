﻿using UnityEngine;
using System.Collections;

public abstract class AbstractButton : AbstractInOut {

	public Texture bgImgOver;
	public Texture bgImgClick;
	protected PopUp targetPopUp;
	public string name;
	enum StateButton {Standard, Clicked, Overed};
	private StateButton state = StateButton.Standard;
	public bool enable = true;

	public void Start() {
		base.Start();
		targetPopUp = this.gameObject.GetComponent<PopUp>();
		if(targetPopUp == null) {
			Debug.Log("Error: you must attach a PopUp component to your game object.");
		}
	}

	public void Update() {
		if(ContainsMouse(GetRectLabel()))
			state = StateButton.Overed;
		else
			state = StateButton.Standard;
	}

	public bool ContainsMouse(Rect r) {
		return r.Contains(new Vector2(Input.mousePosition.x, Screen.height-Input.mousePosition.y));
	}

	public virtual void OnGUI() {
		if(targetPopUp.IsSleeping()) {
			GUI.depth = 2;
		}
		else {
			GUI.depth = 0;
		}
		if(targetPopUp.enable && enable) {
			if(targetToFollow == null) {
				GUISizer.BeginGUI();
				if (GUISizer.ButtonPressed(param)) {
					AkSoundEngine.PostEvent("Play_GUI_click", Camera.main.gameObject);
					ButtonEffects();
				}
				GUISizer.EndGUI();
			}
			else {
				Texture tmpTexture = new Texture();
				switch(state) {
					case StateButton.Standard:
						tmpTexture = bgImg;
					break;
					case StateButton.Overed:
						if(!targetPopUp.IsSleeping())
							tmpTexture = bgImgOver;
						else
							tmpTexture = bgImg;
						break;
					case StateButton.Clicked:
						if(!targetPopUp.IsSleeping())
							tmpTexture = bgImgClick;
						else
							tmpTexture = bgImg;
						break;
				}
				if(state == StateButton.Clicked)
	        		state = StateButton.Standard;
	        	if(GUI.Button(GetRectLabel(), tmpTexture, style)) {
					AkSoundEngine.PostEvent("Play_GUI_click", Camera.main.gameObject);
	        		if(!targetPopUp.IsSleeping()) {
	        			ButtonEffects();
	        			state = StateButton.Clicked;
	        		}
	        	}
	        	GUI.Label(GetRectLabel(), param.text, style);
			}
		}
	}

	public abstract void ButtonEffects();
}
