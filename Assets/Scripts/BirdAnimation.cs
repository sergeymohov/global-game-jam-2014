﻿using UnityEngine;
using System.Collections;

public class BirdAnimation : MonoBehaviour 
{

	bool playerInRange = false;
	public float minDistance = 5f;
	private CController ccontroller;

	private Vector3 spawnPos;
	private bool flying = false;
	Animator anim;

	private float respawnDelay = 120f;

	private Renderer birdRenderer;

	private PopUp popUp;


	void Start()
	{
		transform.parent.eulerAngles += new Vector3(0, Random.Range(-360f,360f), 0);

		spawnPos = transform.parent.position;
		birdRenderer = transform.FindChild("Mesh_TweetBird").renderer;

		popUp = transform.FindChild("NPCPopUp").GetComponent<PopUp>();

		ccontroller = CController.instance;
		anim = gameObject.GetComponent<Animator>();
		StartCoroutine("IdleCycle");
	}

	IEnumerator IdleCycle()
	{
		yield return new WaitForSeconds(Random.Range(3f, 17f));

		switch (Random.Range(0, 2))
		{
			case 0: anim.SetBool("DoEat", true); break;
			case 1: anim.SetBool("DoShake", true); break;
		}

		StartCoroutine("IdleCycle");
		
	}
	
	void OnEat()
	{
		anim.SetBool("DoEat", false);
	}

	void OnShake()
	{
		anim.SetBool("DoShake", false);
	}

	void OnTakeOff()
	{
		int dir1 = 1;
		int dir2 = -1;

		if (Random.Range(0,2) == 0)
			dir1 = 1;
		else
			dir1 = -1;

		if (Random.Range(0,2) == 0)
			dir2 = 1;
		else
			dir2 = -1;

		iTween.MoveTo(transform.parent.gameObject, iTween.Hash("time", 10f, "orienttopath", true, "axis", "y", "looktime", 3f, "easetype", iTween.EaseType.easeInQuart, "position", new Vector3(spawnPos.x + dir1 * Random.Range(50,100f), 300f, spawnPos.x + dir2 * Random.Range(50f,100f)), "oncompletetarget", gameObject, "oncomplete", "Respawn"));
	}

	void OnBecameInvisible()
	{
		if (flying)
		{
			flying = false;
			iTween.Stop(transform.parent.gameObject);
			Respawn();
		}
	}

	void Respawn()
	{	
		

		birdRenderer.enabled = false;

		anim.SetBool("DoEat", false);
		anim.SetBool("DoShake", false);
		anim.SetBool("DoTakeOff", false);

		Invoke("RespawnMe", respawnDelay);
	}

	void RespawnMe()
	{
		transform.parent.position = spawnPos;
		transform.parent.eulerAngles += new Vector3(0, Random.Range(-360f,360f), 0);
		flying = false;
		birdRenderer.enabled = true;
	}

	void Update ()
	{
		playerInRange = (Vector3.Distance(ccontroller.transform.position, transform.position) <= minDistance) ? true : false;

		if (!playerInRange || flying)
		{
			if (popUp)
				popUp.enable = false;
		}
		else
		{
			if (popUp)
				popUp.enable = true;
		}

	}

	//catches callbacks from the interface, launches the bird!
	public void TweetButtonPressed()
	{
		if (!flying)
		{
			flying = true;
			anim.SetBool("DoTakeOff", true);
		}
	}
}
