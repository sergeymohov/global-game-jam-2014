using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;


public class Demo : Singleton<Demo> 
{
        // You need to register your game or application in Twitter to get cosumer key and secret.
    // Go to this page for registration: http://dev.twitter.com/apps/new
    public string CONSUMER_KEY;
    public string CONSUMER_SECRET;

	public bool friendLoaded = false;
	public bool TVeopleLoaded = false;
	
    // You need to save access token and secret for later use.
    // You can keep using them whenever you need to access the user's Twitter account. 
    // They will be always valid until the user revokes the access to your application.
    const string PLAYER_PREFS_TWITTER_USER_ID           = "TwitterUserID";
    const string PLAYER_PREFS_TWITTER_USER_SCREEN_NAME  = "TwitterUserScreenName";
    const string PLAYER_PREFS_TWITTER_USER_TOKEN        = "TwitterUserToken";
    const string PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET = "TwitterUserTokenSecret";

    Twitter.RequestTokenResponse m_RequestTokenResponse;
    public Twitter.AccessTokenResponse m_AccessTokenResponse;
	
	public delegate void ProcessTweetsCallback();

    string m_PIN = "Please enter your PIN here.";
    string m_Tweet = "Please enter your tweet here.";
	
	//This dictionary match frends ID with their own followers count
	public Dictionary<string, Twitter.User> friends = new Dictionary<string, Twitter.User>();
	public Dictionary<int, Twitter.User> tveople = new Dictionary<int, Twitter.User>();

	// Use this for initialization
	void Start() 
    {
		Debug.Log("Demo.start");
		
		if (string.IsNullOrEmpty(CONSUMER_KEY) || string.IsNullOrEmpty(CONSUMER_SECRET))
        {
            Debug.Log("invalid Consumer keys and secret");
        } else {
			 m_AccessTokenResponse = new Twitter.AccessTokenResponse();
			 m_AccessTokenResponse.UserId        = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_ID);
      	 	 m_AccessTokenResponse.ScreenName    = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME);
       		 m_AccessTokenResponse.Token         = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN);
       		 m_AccessTokenResponse.TokenSecret   = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET);

		}
		
		StartCoroutine(Twitter.API.SearchFriends(m_AccessTokenResponse.UserId, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
              		  new Twitter.SearchFriendsCallback(this.OnSearchFriends)));
		
		//StartCoroutine(Twitter.API.SearchTweets("@swannounnet", CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
        //          new Twitter.SearchTweetsCallback(this.OnSearchQuests)));
	}
	
	void OnLevelWasLoaded()
	{
		if (string.IsNullOrEmpty(CONSUMER_KEY) || string.IsNullOrEmpty(CONSUMER_SECRET))
        {
            Debug.Log("invalid Consumer keys and secret");
        } else {
			 m_AccessTokenResponse = new Twitter.AccessTokenResponse();
			 m_AccessTokenResponse.UserId        = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_ID);
      	 	 m_AccessTokenResponse.ScreenName    = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME);
       		 m_AccessTokenResponse.Token         = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN);
       		 m_AccessTokenResponse.TokenSecret   = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET);

		}
		Debug.Log("Demo.start.end---"+CONSUMER_KEY+"---"+m_AccessTokenResponse.Token);
		

		StartCoroutine(Twitter.API.SearchTweets("TVittr", CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
              		  new Twitter.SearchTweetsCallback(this.OnSearchQuests)));


	}


	
	
	// Update is called once per frame
	void Update() 
    {
	}


    //Get access token
    //StartCoroutine(Twitter.API.GetAccessToken(CONSUMER_KEY, CONSUMER_SECRET, m_RequestTokenResponse.Token, m_PIN,
    //               new Twitter.AccessTokenCallback(this.OnAccessTokenCallback)));
	

   
	//post tweet
    //    StartCoroutine(Twitter.API.PostTweet(m_Tweet, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
    //                   new Twitter.PostTweetCallback(this.OnPostTweet)));
    
	
	// Search tweets
    
	
	
    


	#region plugin code
    

    void OnRequestTokenCallback(bool success, Twitter.RequestTokenResponse response)
    {
		m_PIN = "Callback";
        if (success)
        {
			m_PIN = "Success Callback";
            string log = "OnRequestTokenCallback - succeeded";
            log += "\n    Token : " + response.Token;
            log += "\n    TokenSecret : " + response.TokenSecret;
            print(log);

            m_RequestTokenResponse = response;

            Twitter.API.OpenAuthorizationPage(response.Token);
        }
        else
        {
			m_PIN = "Failed callback";
            print("OnRequestTokenCallback - failed.");
        }
    }

    void OnAccessTokenCallback(bool success, Twitter.AccessTokenResponse response)
    {
        if (success)
        {
            string log = "OnAccessTokenCallback - succeeded";
            log += "\n    UserId : " + response.UserId;
            log += "\n    ScreenName : " + response.ScreenName;
            log += "\n    Token : " + response.Token;
            log += "\n    TokenSecret : " + response.TokenSecret;
            print(log);
  
            m_AccessTokenResponse = response;

            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_ID, response.UserId);
            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME, response.ScreenName);
            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_TOKEN, response.Token);
            PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET, response.TokenSecret);
        }
        else
        {
            print("OnAccessTokenCallback - failed.");
        }
    }
	
	#endregion
	
	#region callbacks

    void OnPostTweet(bool success)
    {
        print("OnPostTweet - (or Follow)" + (success ? "succedded." : "failed."));
    }
	
	void OnSearchQuests(bool success, JSONNode rootNode)
	{
		print("OnSearchQuests - " + (success ? "succeeded." : "failed."));
		
	
		if(!success) return;
		
		Debug.Log (rootNode["statuses"].Count);
		
		tveople = new Dictionary<int, Twitter.User>();
		
		for(int i=0; i<rootNode["statuses"].Count; i++)
		{
			JSONNode status = rootNode["statuses"][i];
			Twitter.User newFriend = new Twitter.User();
			newFriend.Id = status["user"]["id"];
			newFriend.Name = status["user"]["screen_name"];
			
			
			
			newFriend.Followers = status["user"]["followers_count"];
			newFriend.AvatarUrl = status["user"]["profile_image_url"];
			
			Twitter.Tweet lastTweet = new Twitter.Tweet();
			lastTweet.id = status["id"];
			lastTweet.text = status["text"];
			lastTweet.user = newFriend.Id;
			
			newFriend.LastTweet = lastTweet;
			
			tveople.Add(i, newFriend);

		}
		
		Debug.Log("TVeople Updated");
		TVeopleLoaded = true;
	}


	
		// Search User
    //StartCoroutine(Twitter.API.SearchUserId("38683264", CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
    //            new Twitter.SearchUserCallback(this.OnSearchUsers)));

	void OnSearchFriends(bool success, JSONNode rootNode)
	{
		//print("Demo.OnSearchFriends - " + (success ? "succeeded." : "failed."));
		
		//Debug.Log(rootNode);
		
		if(!success) return;
		
		//Debug.Log (rootNode["users"].Count+" Friends");
		
		List<string> questTargets = new List<string>();
		
		friends = new Dictionary<string, Twitter.User>();
	
		for(int i=0; i<rootNode["users"].Count; i++)
		{
			JSONNode status = rootNode["users"][i];
			Twitter.User newFriend = new Twitter.User();
			newFriend.Id = status["id"];
			newFriend.Name = status["screen_name"];
			
			if(followQuestTargets.IndexOf(newFriend.Name.ToLower()) >= 0)
			{
				Debug.Log("Follow Quest Completed");
				questTargets.Add(newFriend.Name);
				followQuestTargets.RemoveAt(followQuestTargets.IndexOf(newFriend.Name));
			}
			
			newFriend.Followers = status["followers_count"];
			newFriend.AvatarUrl = status["profile_image_url"];
			
			Twitter.Tweet lastTweet = new Twitter.Tweet();
			lastTweet.id = status["status"]["id"];
			lastTweet.text = status["status"]["text"];
			lastTweet.user = newFriend.Id;
			
			newFriend.LastTweet = lastTweet;
			
			friends.Add(""+i, newFriend);

		}
		
		Debug.Log("FriendList Updated");
		friendLoaded = true;
		
		//If some quest were completed, dispatch some stuff
		//TODO plug that onto Sergey (grouh)
				
		//for(int i=0; i<rootNode["statuses"].Count; i++)
		//{
		//	JSONNode status = rootNode["statuses"][i];
		//	Debug.Log ("@" + status["user"]["name"] + ": " + status["text"]);
		//	Debug.Log (friend["screen_name"]+friend["followers_count"]);
		//}
		
		//DEBUG TEST
//		AddPlayerQuest("krides", "banane");
//		AddPlayerQuest("yakkafo", "lol");
//		AddPlayerQuest("ZachWeiner", "wanna");
//		
//		CheckPlayerQuestStatus(new PlayerQuestStatusCallback(ceciEstUnTestGoFrance));
			
	}
	
	void OnSearchTVeople(bool success, JSONNode rootNode)
	{
		//print("Demo.OnSearchFriends - " + (success ? "succeeded." : "failed."));
		
		Debug.Log("PRINT TWEETS-----------------"+'/'+success+'/'+rootNode);
		
		if(!success) return;
		
		//Debug.Log (rootNode["users"].Count+" Friends");
		
		tveople = new Dictionary<int, Twitter.User>();
	
		for(int i=0; i<rootNode["status"].Count; i++)
		{
			JSONNode status = rootNode["status"][i];
			Twitter.User newFriend = new Twitter.User();
			newFriend.Id = status["id"];
			newFriend.Name = status["screen_name"];
			
			newFriend.Followers = status["followers_count"];
			newFriend.AvatarUrl = status["profile_image_url"];
			
			Twitter.Tweet lastTweet = new Twitter.Tweet();
			lastTweet.id = status["status"]["id"];
			lastTweet.text = status["status"]["text"];
			lastTweet.user = newFriend.Id;
			
			newFriend.LastTweet = lastTweet;
			
			friends.Add(""+i, newFriend);

		}
		
		Debug.Log("TVeople Updated");
		TVeopleLoaded = true;

	}
			
	void ceciEstUnTestGoFrance(List<string> questTargets, List<string> questKeywords)
	{
		//Test friending
		
		//franiborda
		
		//Follow("franiborda");
	}
	
	
	void TweetsLoaded(bool success, JSONNode rootNode)
	{
		Debug.Log("Demo.TweetsLoaded");
		
		fetchingPlayerTweets = false;
		
		StartCoroutine(coroutineTweetsComputing(success, rootNode));
	}
	
	IEnumerator coroutineTweetsComputing(bool success, JSONNode rootNode)
	{
//		Debug.Log("ping coroutine");
//		Debug.Log(playerQuestTargets.Count);
		
		List<string> overPlayerQuestTargets = new List<string>();
		List<string> overPlayerQuestKeywords = new List<string>();
	
		for(int i = 0; i<playerQuestTargets.Count; i++)
		{
			for(int u = 0; u < rootNode.Count; u++)
			{
				JSONNode tweet = rootNode[u];
				string txt = (string) tweet["text"];
//				Debug.Log("1 Demo.cs CoroutineTweetsComputing "+txt	);
//				Debug.Log("2 Demo.cs CoroutineTweetsComputing "+playerQuestTargets[i].ToLower());		
//				Debug.Log("3 Demo.cs CoroutineTweetsComputing "+playerQuestKeywords[i].ToLower());
				if(txt.ToLower().Contains(playerQuestTargets[i].ToLower()) && txt.ToLower().Contains(playerQuestKeywords[i].ToLower()))
				{
//					Debug.Log("quest complete");
					overPlayerQuestTargets.Add(playerQuestTargets[i]);
					overPlayerQuestKeywords.Add(playerQuestKeywords[i]);
					
					playerQuestTargets.RemoveAt(i);
					playerQuestKeywords.RemoveAt(i);
					
					break;
				}
			}
		}
		
		if(fetchCallback != null)
		{
			fetchCallback(overPlayerQuestTargets, overPlayerQuestKeywords);
			fetchCallback = null;
		}
		
		yield return null;
	}
	
	
	#endregion
	
	
	public void Tweet(string tweetTxt)
	{
		//post tweet
        StartCoroutine(Twitter.API.PostTweet(tweetTxt, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
                       new Twitter.PostTweetCallback(this.OnPostTweet)));
	}
	
	public void Follow(string user_name)
	{
		StartCoroutine(Twitter.API.Follow(user_name, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
                       new Twitter.PostTweetCallback(this.OnPostTweet)));
	}
	
	#region Quest Checks
	
	private List<string> playerQuestTargets = new List<string>();
	private List<string> playerQuestKeywords = new List<string>();
	
	private bool fetchingPlayerTweets = false;
	private PlayerQuestStatusCallback fetchCallback; //THIS IS REALLY REALLY DIRTY YOU BAD BAD BOY
	
	public delegate void LoadTweetsCallback(bool success, JSONNode node);
	public delegate void PlayerQuestStatusCallback(List<string> questTargets, List<string> questKeywords);
	
	public void CheckPlayerQuestStatus(PlayerQuestStatusCallback callback)
	{
		
		if(!fetchingPlayerTweets){
			//If we are not already fetching
			
			fetchingPlayerTweets = true;
			fetchCallback = callback;
			
			StartCoroutine(Twitter.API.LoadUserTweets(m_AccessTokenResponse.UserId, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
               		  new Twitter.LoadTweetsCallback(this.TweetsLoaded)));
			
		}
	}
	
	public void AddPlayerQuest(string target, string keyword)
	{
		Debug.Log("Quest queud/"+target+"/"+keyword);
		playerQuestTargets.Add(target.ToLower());
		playerQuestKeywords.Add(keyword.ToLower());
	}
	
	public List<string> followQuestTargets = new List<string>();
	
	public void AddFollowQuest(string target)
	{
		followQuestTargets.Add(target.ToLower());
	}
	
	public delegate void FollowQuestStatusCallback(List<string> questTargets);
	
	public void CheckFollowQuestStatus()
	{
		//Update Friends Lists
		
		StartCoroutine(Twitter.API.SearchFriends(m_AccessTokenResponse.UserId, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
              		  new Twitter.SearchFriendsCallback(this.OnSearchFriends)));
	}

	private List<string> FriendQuestTargets = new List<string>();
	private List<string> FriendQuestKeywords = new List<string>();

	public void AddFriendQuest(string target, string keyword)
	{
		FriendQuestTargets.Add(target);
		FriendQuestKeywords.Add(keyword);
	}

	public void CheckFriendQuests()
	{
		//@_lcarpentier + ( banane + #ggj14)

		if (FriendQuestKeywords.Count == 0) return;

		string request = "@";
		request += PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME);
		request += " + ( ";

		for(int i = 0; i < FriendQuestKeywords.Count; i++)
		{
			request +=FriendQuestKeywords[i];

			if(i < FriendQuestKeywords.Count-1 ){request += " OR ";}
		}

		request += " )";

		Debug.Log("My Request: ---- "+request);

		StartCoroutine(Twitter.API.SearchTweets(request, CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
		                                        new Twitter.SearchTweetsCallback(this.OnCheckQuest)));
	}

	void OnCheckQuest(bool success, JSONNode rootNode)
	{
		print("OnCheckQuest - " + (success ? "succeeded." : "failed."));
		
		
		if(!success) return;
		
		Debug.Log (rootNode);

		List<int> fulfillesQuestIds = new List<int>();
		List<int> followers = new List<int>();

		for(int i=0; i<rootNode["statuses"].Count; i++)
		{
			//For Each tweet
			JSONNode status = rootNode["statuses"][i];
			Twitter.User newFriend = new Twitter.User();
			newFriend.Id = status["user"]["id"];
			newFriend.Name = status["user"]["screen_name"];
			
			
			
			newFriend.Followers = status["user"]["followers_count"];
			newFriend.AvatarUrl = status["user"]["profile_image_url"];
			
			Twitter.Tweet lastTweet = new Twitter.Tweet();
			lastTweet.id = status["id"];
			lastTweet.text = status["text"];
			lastTweet.user = newFriend.Id;
			
			newFriend.LastTweet = lastTweet;

			for(int u=0; u < FriendQuestTargets.Count; u++)
			{
				//for each target
				//check if tweet quest


				if(newFriend.Name.ToLower() == FriendQuestTargets[u].ToLower())
				{
					//the tweet was written by the target
					if(newFriend.LastTweet.text.Contains(FriendQuestKeywords[u]))
					{
						//the tweet is a quest
						if(!(fulfillesQuestIds.IndexOf(u) >= 0))
						{	
							fulfillesQuestIds.Add(u);
							followers.Add(int.Parse(newFriend.Followers));
						}
					}
				}

			}
		}

		Debug.Log(fulfillesQuestIds.Count);
	

		//Handle score screen

		int localScore = 0;

		for(int y=0; y<fulfillesQuestIds.Count; y++)
		{
			localScore+= followers[y];
		}

		string txtScore = "You completed "+fulfillesQuestIds.Count+" TVittQuest and won "+localScore+" candies";
		
		CController.instance.candiesCollected += localScore;

		//Delete from quests

		for (int z = fulfillesQuestIds.Count - 1; z >= 0 ; z -- )
		{
			FriendQuestKeywords.RemoveAt(z);
			FriendQuestTargets.RemoveAt(z);
		}

		if(scoreScreen)
		{
			scoreScreen.enable = true;
			scoreScreen.output = txtScore;
		}

	}

	public PopUp scoreScreen;
	
	#endregion
	
	
	
}
