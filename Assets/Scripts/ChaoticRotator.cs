﻿using UnityEngine;
using System.Collections;

public class ChaoticRotator : MonoBehaviour {

	private float nextActionTime = 0.0f;
	private float randomOffset;
	public float period = 0.05f;

	// Use this for initialization
	void Start () {
		randomOffset = Random.value * period;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > nextActionTime + randomOffset) {
			nextActionTime += period;
			transform.Rotate(Random.value * 360, Random.value * 360, Random.value * 360);
		}
	}
}
