﻿using UnityEngine;
using System.Collections;

public class ExclamationMark : MonoBehaviour 
{
	private bool killed = false;

	public void ExclamationHide()
	{
		renderer.enabled = false;
		AkSoundEngine.PostEvent("Mute_NPC_alert", gameObject);
	}

	public void ExclamationShow()
	{
		if (!killed)
		{
			AkSoundEngine.PostEvent("Restore_NPC_alert", gameObject);
			renderer.enabled = true;
		}
	}

	public void ExclamationKill()
	{
		renderer.enabled = false;
		killed = true;
		AkSoundEngine.PostEvent("Mute_NPC_alert", gameObject);
	}

	void Start () 
	{
		AkSoundEngine.PostEvent("Play_NPC_alert", gameObject);
		iTween.PunchScale(gameObject, iTween.Hash("amount",new Vector3(1,1,1), "delay", 1f, "time",1f, "oncomplete", "Repunch1"));
	}

	void Repunch1()
	{
		AkSoundEngine.PostEvent("Play_NPC_alert", gameObject);
		iTween.PunchScale(gameObject, iTween.Hash("amount",new Vector3(1,1,1), "delay", 1f, "time",1f, "oncomplete", "Repunch2"));
	}

	void Repunch2()
	{
		AkSoundEngine.PostEvent("Play_NPC_alert", gameObject);
		iTween.PunchScale(gameObject, iTween.Hash("amount",new Vector3(1,1,1), "delay", 1f, "time",1f, "oncomplete", "Repunch1"));
	}
	
}
