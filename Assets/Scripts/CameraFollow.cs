﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public Transform followTransform;
	public float cameraOffset = 50f;
	void FixedUpdate () 
	{
		transform.position = new Vector3(followTransform.position.x - 50f, transform.position.y, followTransform.position.z);
	}
}
