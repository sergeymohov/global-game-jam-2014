/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID DIM_MUSIC = 4178126369U;
        static const AkUniqueID MUTE_NPC_ALERT = 2876198305U;
        static const AkUniqueID PLAY_AMBIANCES = 1193561773U;
        static const AkUniqueID PLAY_BIRD_EAT = 3400702984U;
        static const AkUniqueID PLAY_BIRD_FLAP = 3600390579U;
        static const AkUniqueID PLAY_BIRD_TWEET = 4031277573U;
        static const AkUniqueID PLAY_BIRD_TWEETS = 1191110060U;
        static const AkUniqueID PLAY_CREEK = 135230898U;
        static const AkUniqueID PLAY_GUI_CLICK = 1244026894U;
        static const AkUniqueID PLAY_MUSIC = 2932040671U;
        static const AkUniqueID PLAY_NPC_ALERT = 722044104U;
        static const AkUniqueID PLAY_NPC_SPEECH = 1341844128U;
        static const AkUniqueID PLAY_STEP = 99206220U;
        static const AkUniqueID PLAY_TESTQUACK = 2366170371U;
        static const AkUniqueID PLAY_WATERFALL = 467174588U;
        static const AkUniqueID RESTORE_MUSIC = 3357583321U;
        static const AkUniqueID RESTORE_NPC_ALERT = 251336746U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace GAMESTATE
        {
            static const AkUniqueID GROUP = 4091656514U;

            namespace STATE
            {
                static const AkUniqueID INGAME = 984691642U;
                static const AkUniqueID POPUP = 2123891029U;
            } // namespace STATE
        } // namespace GAMESTATE

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID GGJ_TWITTERSAGA = 2171168807U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID GUI = 915214414U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID MUS = 712897226U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
